<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Post;
use App\Tag;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('{user}/posts/', function (User $user) {
    // return $user;
    $posts = $user->posts;
    return view('users.show', [
        'user' => $user, 
        'posts' => $posts
    ]);
});

Route::get('posts/{post}', function (Post $post) {
    // return $post;
    // $post = $user->posts;
    return view('posts.show', [
        'post' => $post
    ]);
});

Route::get('tags/{tag}/posts', function (Tag $tag) {
    // return $post;
    return $tag->posts;
    // return view('posts.show', [
    //     'tag' => $tag
    // ]);
});
