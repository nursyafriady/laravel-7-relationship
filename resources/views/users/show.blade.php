@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $user->name }}</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Deksripsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($posts as $post)
                                <tr>
                                    <td><a href="/posts/{{ $post->id }}">{{ $post->title ?? ''  }}</a></td>
                                    <td>{{ $post->body ?? '' }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center">Tidak Ada</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
