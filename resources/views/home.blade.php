@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($posts as $post) 
            <div class="col-md-6">                      
                <div class="card mb-4">
                    <div class="card-header"> {{ $post->title}} </div>
                    <div class="card-body">
                        {{ $post->body}}
                    </div>
                    <div class="card-footer">                        
                            @foreach($post->tags as $tag) 
                                <div class="btn btn-sm btn-success">
                                    {{ $tag->name ?? ''}}
                                </div> 
                            @endforeach                                               
                    </div>
                </div>            
            </div>
        @endforeach
    </div>
</div>
@endsection
